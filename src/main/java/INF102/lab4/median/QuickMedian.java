package INF102.lab4.median;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.swing.text.html.HTMLDocument.RunElement;

public class QuickMedian implements IMedian {
    int k = 0;
    int count = 0;

    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        Random random = new Random();
        int rand = random.nextInt(listCopy.size());
        T pivot = listCopy.get(rand);

        if (listCopy.size() == 1) {
            count = 0;
            return listCopy.get(0);
        }

        // getting the k
        if (this.count == 0) {
            k = list.size() / 2;
            count++;
        }

        List<T> smaller = new ArrayList<T>();
        List<T> larger = new ArrayList<T>();
        List<T> PivList = new ArrayList<T>();

        // sorting the list
        for (T t : listCopy) {
            if (t.compareTo(pivot) < 0) {
                smaller.add(t);
            } else if (t.compareTo(pivot) > 0) {
                larger.add(t);
            } else {
                PivList.add(t);
            }
        }

        if (k <= smaller.size()) {
            smaller.addAll(PivList);
            if (smaller.get(k).equals(pivot)) {
                count = 0;
                return pivot;
            }
            return median(smaller);
        }
        // k is bigger than small
        else {
            PivList.addAll(larger);
            k = k - smaller.size();
            if (PivList.get(k).equals(pivot)) {
                count = 0;
                return pivot;
            }
            return median(PivList);
        }

    }
    
}
