package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        for (int i = 1; i < list.size(); i++ ) {
            for (int j = 0; j < i; j ++ ) {
                if (list.get(j).compareTo(list.get(i)) == 1 ) {
                    T tmp = list.get(j);
                    list.set(j, list.get(i)); 
                    list.set(i, tmp);
                }
            }
        }
    }
    
}
